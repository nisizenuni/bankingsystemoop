import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

enum TransactionType  {
    TRANSFER,
    WITHDRAWAL,
    DEPOSIT,
}

class BalanceNotAllowedException extends Exception {
public  BalanceNotAllowedException(String message) {
    super(message);
}
}

class AccountNotFoundException extends Exception{
    public AccountNotFoundException(Integer id) {
        super(String.format("Category with id: %d is not found", id));
    }
}

class BankNotFoundException extends Exception{
    public BankNotFoundException(String message) {
        super(message);
    }
}

  class Transaction {
    Double amount;
    Integer myAccount;
    Integer hisAccount;
    TransactionType transactionType;

      public Transaction(Double amount, Integer myAccount, Integer hisAccount, TransactionType transactionType) {
          this.amount = amount;
          this.myAccount = myAccount;
          this.hisAccount = hisAccount;
          this.transactionType = transactionType;
      }

      public Transaction() {

      }

      public Double getAmount() {
          return amount;
      }

      public void setAmount(Double amount) {
          this.amount = amount;
      }

      public Integer getMyAccount() {
          return myAccount;
      }

      public void setMyAccount(Integer myAccount) {
          this.myAccount = myAccount;
      }

      public Integer getHisAccount() {
          return hisAccount;
      }

      public void setHisAccount(Integer hisAccount) {
          this.hisAccount = hisAccount;
      }

      public TransactionType getTransactionType() {
          return transactionType;
      }

      public void setTransactionType(TransactionType transactionType) {
          this.transactionType = transactionType;
      }
  }

class Account  {
    private Integer Id;
    private String username;
    private Double balance;


    //TODO check here
    public Account(Integer id, String username, Double balance) throws BalanceNotAllowedException {
        this.Id = id;
        this.username = username;
        this.balance = balance;

    }

    public Integer getId() {
        return Id;
    }

    public String getUsername() {
        return username;
    }

    public Double getBalance() {
        return balance;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public void DepositFunction(double amount) {
        setBalance(getBalance()+amount);
    }

    public void WithdrawFunction(double amount) {
        setBalance(getBalance()-amount);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(Id).append(" ").append(username).append(" ").append(balance).append("\n");
        return sb.toString();
    }
}

class Bank extends Transaction {
    String name;
    List<Account> accountList;
    Double fee;



    public Bank(Double amount, Integer myAccount, Integer hisAccount, TransactionType transactionType, String name, List<Account> accountList, Double fee) {
        super(amount, myAccount, hisAccount, transactionType);
        this.name = name;
        this.accountList = accountList;
        this.fee = fee;

    }

    public Bank(String name, List<Account> accountList, Double fee) {
        this.name = name;
        this.accountList = accountList;
        this.fee = fee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(" ").append(fee).append(accountList).append("\n");
        return sb.toString();
    }

    public void TransactionFunction(TransactionType transactionType) throws AccountNotFoundException {
       Account account1 = accountList.stream()
               .filter(account -> account.getId().equals(myAccount))
               .findFirst().orElseThrow(()->new AccountNotFoundException(myAccount));
       Account account2 = accountList.stream()
               .filter(account -> account.getId().equals(hisAccount))
               .findFirst().orElseThrow(()->new AccountNotFoundException(hisAccount));

        switch (transactionType) {
            case DEPOSIT -> {
                account1.setBalance(account1.getBalance()+amount);
            }
            case WITHDRAWAL -> {
                account1.setBalance(account1.getBalance()-amount-fee);
            }
            case TRANSFER -> {
                account1.setBalance(account1.getBalance()-amount-fee);
                account2.setBalance(account2.getBalance()+amount);

            }
            default -> {
                System.out.println("There was an error please try again");
            }
        }



    }




}







public class LinkPlusBankingSystem {


    public static void main(String[] args) throws BalanceNotAllowedException, AccountNotFoundException, IOException, BankNotFoundException {


        List<Account> accounts = new ArrayList<>();
        List<Bank> banks = new ArrayList<>();

        System.out.println("======CREATING ACCOUNTS=======\n");
        for(int i=0;i<11;i++) {
            accounts.add(new Account(i,"Account" + i,(i+1)*10.0));
        }

        for(Account a : accounts) {
            System.out.println(a.toString());
        }

        System.out.println("======ACCOUNTS CREATED SUCCESSFULLY=======\n");

        TransactionType DEPOSIT = TransactionType.DEPOSIT;
        TransactionType WITHDRAWAL = TransactionType.WITHDRAWAL;
        TransactionType TRANSFER = TransactionType.TRANSFER;


        /*
        System.out.println("======CREATING BANKS=======\n");
        for (int i=0;i<=3;i++) {
            banks.add(new Bank("Bank" + i,accounts,(i+1)*1.1));
        }

        for(Bank b : banks) {
            System.out.println(b.toString());
        }

        System.out.println("======BANKS CREATED SUCCESSFULLY=======\n");

         */



        Account account1 = accounts.stream()
                .filter(account -> account.getId().equals(3))
                .findFirst().orElseThrow(()->new AccountNotFoundException(3));

        Account account5 = accounts.stream()
                .filter(account -> account.getId().equals(5))
                .findFirst().orElseThrow(()->new AccountNotFoundException(5));

        Account account7 = accounts.stream()
                .filter(account -> account.getId().equals(7))
                .findFirst().orElseThrow(()->new AccountNotFoundException(7));

        Account account8 = accounts.stream()
                .filter(account -> account.getId().equals(8))
                .findFirst().orElseThrow(()->new AccountNotFoundException(8));




        System.out.println("====== TESTING WITHDRAWAL AND DEPOSIT ==================");


        System.out.println("Amount before Deposit " + account1.getBalance() + "\n");
        Bank b = new Bank(100.0,3,4,DEPOSIT,"Stopanska",accounts,5.0);
        b.TransactionFunction(DEPOSIT);
        System.out.println("Amount after Deposit " + account1.getBalance() + "\n");


        System.out.println("Amount before Withdrawal " + account5.getBalance() + "\n");
        Bank b1 = new Bank(50.0,5,6,WITHDRAWAL,"NLB",accounts,2.5);
        b1.TransactionFunction(WITHDRAWAL);
        System.out.println("Amount after Withdrawal " + account5.getBalance() + "\n");


        System.out.println("====== TESTING TRANSFER  ======= \n");
        System.out.println("Amount before Transfer\nAccount From " + account7.getBalance() + "\nAccount To " + account8.getBalance() + "\n");
        Bank b2 = new Bank(10.0,7,8,TRANSFER,"NLB",accounts,10.0);
        b2.TransactionFunction(TRANSFER);
        System.out.println("Amount after Transfer\nAccount From " + account7.getBalance() + "\nAccount To " + account8.getBalance() + "\n");




    }
}
